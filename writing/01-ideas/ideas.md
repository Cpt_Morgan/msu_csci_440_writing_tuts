# Team Members

* Morgan Johnson
* Michaela Lightner
* James Tomasko
* Logan Pappas

# Idea 1

## Mapping the Horizons: A Beginners Guide to Basic SQl
The objective for this tutorial will be familiarizing the layperson with basic SQL commands such as JOIN and SELECT.
This empowers the user by giving them a foundation with which to handle operations concerning large datasets, which is a very useful skill.

# Idea 2

## ER Diagrams for Dummies 
In this tutorial, a user will be presented with a general description of a K-12 school and guided on how to translate that description into an ER, or Entity-Relationship, diagram. 
This gently familiarizes the user with some of the basic concepts of database design, giving them a solid ground on which to stand on and learn more advanced concepts going forward. 


# Idea 3

## Relational data modeling
This guide features a basic introduction  to relational database modeling, with a key focus on optimizing the design of a relational database to enhance performance and stability. 
This topic is important to know and understand as it can be easy to make initial design mistakes with a relational database that can lead to performance issues, along with stability issues when it comes to updating the database. 
