# ER Diagrams for Dummies Outline

### Part 1: A quick refresher on ER diagram syntax
#### Person initially working on this part: Kala 
Millman said that we can assume the reader is familiar with the basic elements of an ER diagram, i.e. a square for an Entity and a circle for an attribute. In this section we can likely do a quick refresher on these, i.e. post a labeled picture of them. 

### Part 2: The basic elements of our diagram
#### Person initially working on this part: Morgan 
As the overall goal is to model a K-12 school, in this first section we can likely then figure out what entities will be in our model, along with what attributes they will have. I.e. we will have a student entity, they will have a name, etc. No relationships at this point yet, we are kinda just getting a list of the entities and attributes for the next step. 

### Part 3: Getting serious with relationships
#### Person initially working on this part:  
At this point we could, in a way similar to part 2, list out the *relationships* that will be in our diagram, i.e. a student is a STUDENT_OF one or more classes, and each class has a TEACHES relationship with one or maybe more teachers. We do not need to be too exhaustive here, especially with this first draft. 

### Part 4: Connecting the dots
#### Person initially working on this part: Logan 
Here we will take the information we have so far and actually put it in the form of a full diagram. 


## Misc. Notes:
As this is a tutorial, we do not want to just make a journal about this process. Whenever possible, try to prompt the user to try something on their own, like list the entities they think would be important to model for a school, and then we can show our process of going about that. 